package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.repository.ContractSeal;
import id.ac.ui.cs.advprog.tutorial2.command.repository.ContractSeal;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.*;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spells;
    private int currentIndex;
    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell s : spells){
            s.cast();
            currentIndex = spells.indexOf(s);
        }

    }

    @Override
    public void undo() {
        for (int i=currentIndex; i>=0; i--){
            spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
