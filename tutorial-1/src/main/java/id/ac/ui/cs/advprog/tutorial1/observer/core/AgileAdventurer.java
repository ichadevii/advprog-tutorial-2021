package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.List;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me

    @Override
    public void update() {
        if (((guild.getQuestType()).equals("R")) || ((guild.getQuestType()).equals("D"))){
            List<Quest> newQuest = getQuests();
            newQuest.add(guild.getQuest());
        }
    }
}
