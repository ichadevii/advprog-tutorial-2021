package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.List;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me


    @Override
    public void update() {
        List<Quest> newQuest = getQuests();
        newQuest.add(guild.getQuest());
    }
}
