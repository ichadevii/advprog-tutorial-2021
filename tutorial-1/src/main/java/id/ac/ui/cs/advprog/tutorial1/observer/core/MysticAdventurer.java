package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.List;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me


    @Override
    public void update() {
        if (((guild.getQuestType()).equals("E")) || ((guild.getQuestType()).equals("D"))){
            List<Quest> newQuest = getQuests();
            newQuest.add(guild.getQuest());
        }
    }
}
