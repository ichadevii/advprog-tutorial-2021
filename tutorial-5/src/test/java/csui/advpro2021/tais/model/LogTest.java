package csui.advpro2021.tais.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

public class LogTest {
    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() throws Exception {
        LocalDateTime startTime = LocalDateTime.of(2020, 10, 4, 10, 20, 55);
        LocalDateTime endTime = LocalDateTime.of(2020, 11, 10, 10, 21, 1);
        this.log = new Log(1, startTime, endTime, "Habis ngasis");
        this.mahasiswa = new Mahasiswa("1906190619", "Kim Seokjin", "kim@cs.ui.ac.id", "4", "08123456789");
        this.log.setMahasiswa(this.mahasiswa);

    }

    @Test
    public void testRelationshipMahasiswaLog() {
        Assertions.assertEquals(mahasiswa, log.getMahasiswa());
    }
}
