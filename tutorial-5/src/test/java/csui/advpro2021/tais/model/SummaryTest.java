package csui.advpro2021.tais.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SummaryTest {

    private Summary summary;

    @BeforeEach
    public void setUp() throws Exception {
        summary = new Summary("Januari", 1.0, 350.0);
    }

    @Test
    public void testSummaryHasGetBulanMethod() {
        Assertions.assertEquals("Januari", summary.getBulan());
    }

    @Test
    public void testSummaryHasGetJamKerjaMethod() {
        Assertions.assertEquals(1.0, summary.getJamKerja());
    }

    @Test
    public void testSummaryHasGetPembayaranMethod() {
        Assertions.assertEquals(350.0, summary.getPembayaran());
    }
}
