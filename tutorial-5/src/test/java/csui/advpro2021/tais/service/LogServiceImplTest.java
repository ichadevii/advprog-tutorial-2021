package csui.advpro2021.tais.service;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;

    private Mahasiswa mahasiswa;

    List<Log> listlog;
    @BeforeEach
    public void setUp() {
        LocalDateTime start = LocalDateTime.of(2020, 10, 4,
                10, 20, 55);
        LocalDateTime end = LocalDateTime.of(2020, 11, 10,
                10, 21, 1);
        log = new Log(1, start, end, "Habis ngasis");
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4", "081317691718");

        listlog = new ArrayList<Log>();
        mahasiswa.setLogList(listlog);
        mahasiswaRepository.save(mahasiswa);

    }

    @Test
    void testServiceGetListLog() {
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getListLog();
        Assertions.assertIterableEquals(listLog, listLogResult);
    }



    @Test
    void testServiceCreateLog() {
        lenient().when(logService.createLog(log)).thenReturn(log);
        Log resultLog = logService.createLog(log);
        Assertions.assertEquals(log.getIdLog(), resultLog.getIdLog());
    }

    @Test
    void testServiceDeleteLog() {
        logService.createLog(log);
        logService.deleteLogByidLog(log.getIdLog());
        Assertions.assertEquals(null, logService.getLogByidLog(log.getIdLog()));
    }

    @Test
    void testServiceUpdateMataKuliah() {
        logService.createLog(log);
        String deskripsi = "Halo";
        log.setDeskripsi(deskripsi);
        Log expectedLog = log;
        expectedLog.setDeskripsi(deskripsi);
        Log resultLog = logService.updateLog(log.getIdLog(), log);
        Assertions.assertEquals(expectedLog.getDeskripsi(), resultLog.getDeskripsi());
    }

    @Test
    public void testServiceSummary() throws Exception {
        Mahasiswa mahasiswa2 = mahasiswaRepository.findByNpm(mahasiswa.getNpm());
        lenient().when(mahasiswa2).thenReturn(mahasiswa);
        assertNotEquals(null,logService.laporanPembayaran(mahasiswa.getNpm(), "Oktober"));
    }

}
