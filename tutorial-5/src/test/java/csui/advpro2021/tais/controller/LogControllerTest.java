package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Arrays;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.time.LocalDateTime;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;



@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp(){
        LocalDateTime start = LocalDateTime.of(2020, 10, 4,
                10, 20, 55);
        LocalDateTime end = LocalDateTime.of(2020, 11, 10,
                10, 21, 1);
        log = new Log(1, start, end, "Habis ngasis");
        mahasiswa = new Mahasiswa("1906190619", "Kim Seokjin", "kim@cs.ui.ac.id", "4",
                "08123456789");

    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

//    @Test
//    public void testControllerPostLog() throws Exception{
//        mvc.perform(post("/log/" + log.getIdLog() + "/dicatat/" + mahasiswa.getNpm())
//                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(log)))
//                .andExpect(status().isOk());
//    }

    @Test
    public void testControllerGetListLog() throws Exception{

        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/log").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value("1"));
    }

    @Test
    public void testControllerGetLogByIdLog() throws Exception{

        when(logService.getLogByidLog(1)).thenReturn(log);
        mvc.perform(get("/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Habis ngasis"));
    }

    @Test
    public void testControllerGetNonExisLog() throws Exception{
        mvc.perform(get("/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerDeleteLog() throws Exception{
        logService.createLog(log);
        mvc.perform(delete("/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerUpdateLog() throws Exception{

        logService.updateMahasiswa(mahasiswa.getNpm(), log);

        //Update deskripsi log
        log.setDeskripsi("Habis tutorial");

        when(logService.updateLog(log.getIdLog(), log)).thenReturn(log);

        mvc.perform(put("/log/1").contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(mahasiswa)))
                .andExpect(status().isOk());
    }

//    @Test
//    public void testServiceSummary() throws Exception {
//        when(logService.updateMahasiswa(mahasiswa.getNpm(), log)).thenReturn(log);
//        mvc.perform(get("/log/laporanPembayaran/{npm}/bulan/{bulan}", "1906190619", "October")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk());
//    }


}
