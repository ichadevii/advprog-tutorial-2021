package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "summary")
@Data
@NoArgsConstructor
public class Summary {

    @Id
    @Column(name = "bulan")
    private String bulan;

    @Column(name = "jamKerja")
    private double jamKerja;

    @Column(name = "pembayaran")
    private double pembayaran;

    public Summary(String bulan, double jamKerja, double pembayaran) {
        this.bulan = bulan;
        this.jamKerja = jamKerja;
        this.pembayaran = pembayaran;
    }
}
