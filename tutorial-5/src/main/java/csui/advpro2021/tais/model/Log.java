package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {

    @Id
    @Column(name = "idLog", updatable = false, nullable = false)
    private int idLog;

    @Column(name = "startTime")
    private LocalDateTime startTime;

    @Column(name = "endTime")
    private LocalDateTime endTime;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="mahasiswa_npm")
    private Mahasiswa mahasiswa;

    public Log(int idLog, LocalDateTime startTime, LocalDateTime endTime, String deskripsi) {
        this.idLog = idLog;
        this.startTime = startTime;
        this.endTime = endTime;
        this.deskripsi = deskripsi;
    }
}
