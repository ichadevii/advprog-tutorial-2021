### Compound Pattern

### Requirement soal

#### MAHASISWA
1. Mahasiswa memiliki atribut NPM(unik), Nama, Email, ipk, noTelp
2. Mahasiswa dapat mendaftar ke satu mata kuliah dan langsung diterima
atau Mahasiswa hanya dapat menjadi asisten di satu mata kuliah.
3. Mahasiswa yang sudah menjadi asisten di suatu mata kuliah tidak bisa dihapus
4. Setiap mahasiswa bekerja akan dicatat di log
5. Mahasiswa memiliki laporan pembayaran setiap bulannya

#### MATA_KULIAH
1. Mata kuliah memiliki atribut kodeMatkul(unik), namaMatkul, dan prodi
2. Satu mata kuliah dapat menerima banyak mahasiswa
3. Mata kuliah yang sudah ada asisten tidak dapat dihapus

#### LOG
1. Entity Log memiliki atribut int idLog(unik), LocalDateTime startTime, LocalDateTime endTime, dan String deskripsi
2. Banyak log dicatat mahasiswa yang merepresentasikan pekerjaan yang telah dilakukan mahasiswa
3. Log dapat dibuat (dan disimpan sistem), diperbaharuhi, dan dihapus.
4. Log-log pada bulan yang sama dijadikan Summary

#### SUMMARY
1. Entity Summary memiliki atribut String bulan, double jamKerja, dan int pembayaran
2. pembayaran 350 Greil/jam 
3. Log bisa kurang dari 1 jam 

### RELATIONSHIP
![Tutorial5Adprog__1_](/uploads/5f81cee522be224ac6f528ad5472aabc/Tutorial5Adprog__1_.png)
1. MATA_KULIAH one-to-many MAHASISWA
2. MAHASISWA one-to-many LOG

### TAMBAHAN PATH
1. mahasiswa/{npm}/mengasdosi/{kodeMatkul} 
2. log/{idLog}/dicatat/{npm}
3. log/laporanPembayaran/{npm}/bulan/{bulan}
