package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenIngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiIngredientsFactory;

public class SnevnezhaShirataki extends Menu {
    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami
    private RamenIngredientsFactory ingredientsFactory;
    public SnevnezhaShirataki(String name){
        super(name, new SnevnezhaShiratakiIngredientsFactory());
    }

}