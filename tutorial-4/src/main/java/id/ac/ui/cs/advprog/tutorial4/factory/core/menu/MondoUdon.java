package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonIngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenIngredientsFactory;

public class MondoUdon extends Menu {
    //Ingridients:
    //Noodle: Udon
    //Meat: Chicken
    //Topping: Cheese
    //Flavor: Salty
    private RamenIngredientsFactory ingredientsFactory;
    public MondoUdon(String name){
        super(name, new MondoUdonIngredientsFactory());
    }

}