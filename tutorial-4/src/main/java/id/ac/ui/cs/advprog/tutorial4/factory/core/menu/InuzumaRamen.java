package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenIngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenIngredientsFactory;

public class InuzumaRamen extends Menu {
    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy
    private RamenIngredientsFactory ingredientsFactory;
    public InuzumaRamen(String name){
        super(name, new InuzumaRamenIngredientsFactory());
    }
}