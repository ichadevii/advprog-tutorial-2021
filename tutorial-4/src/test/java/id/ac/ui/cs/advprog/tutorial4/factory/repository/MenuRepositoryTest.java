package id.ac.ui.cs.advprog.tutorial4.factory.repository;


import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Repository
public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @Mock
    private List<Menu> listItem;

    private Menu menu;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
        listItem = new ArrayList<>();
        menu = new LiyuanSoba("sobami");
        listItem.add(menu);
    }

    @Test
    public void whenMenuRepoFGetMenusShouldReturnMenuList() {
        ReflectionTestUtils.setField(menuRepository, "list", listItem);
        List<Menu> acquiredMenu = menuRepository.getMenus();

        assertThat(acquiredMenu).isEqualTo(listItem);
    }

    @Test
    public void whenMenuRepoAddShouldAddItToList() {
        ReflectionTestUtils.setField(menuRepository, "list", listItem);
        Menu menu2 = new InuzumaRamen("ramen");

        menuRepository.add(menu2);
        List<Menu> acquiredMenu = menuRepository.getMenus();
        boolean isContainNewMenu = acquiredMenu.contains(menu2);

        assertTrue(isContainNewMenu);
    }


}
