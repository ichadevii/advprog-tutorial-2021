package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MushroomTest {
    private Class<?> mushroomClass;
    private Mushroom mushroom;

    @BeforeEach
    public void setup() throws Exception {
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
        mushroom = new Mushroom();
    }

    @Test
    public void testIsMushroomConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mushroomClass.getModifiers()));
    }

    @Test
    public void testMushroomIsATopping() {
        Collection<Type> interfaces = Arrays.asList(mushroomClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

//    public Flavor createFlavor();
//    public Meat createMeat();
//    public Noodle createNoodle();
//    public Topping createTopping();

    @Test
    public void testMushroomOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = mushroomClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testMushroomReturnGetDescriptionMethod() {
        String result = mushroom.getDescription();
        assertEquals("Adding Shiitake Mushroom Topping...", result);
    }
}