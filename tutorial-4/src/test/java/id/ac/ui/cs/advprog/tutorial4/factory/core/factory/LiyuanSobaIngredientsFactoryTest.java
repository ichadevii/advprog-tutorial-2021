package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class LiyuanSobaIngredientsFactoryTest {
    private Class<?> liyuanSobaIngredientsFactoryClass;

    @InjectMocks
    private LiyuanSobaIngredientsFactory liyuanSobaIngredientsFactory;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaIngredientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaIngredientsFactory");
        liyuanSobaIngredientsFactory = new LiyuanSobaIngredientsFactory();
    }

    @Test
    public void testIsLiyuanSobaIngredientsFactoryConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaIngredientsFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientsFactoryIsARamenIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaIngredientsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenIngredientsFactory")));
    }

//    public Flavor createFlavor();
//    public Meat createMeat();
//    public Noodle createNoodle();
//    public Topping createTopping();

    @Test
    public void testLiyuanSobaIngredientsFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = liyuanSobaIngredientsFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientsFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = liyuanSobaIngredientsFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientsFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = liyuanSobaIngredientsFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientsFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = liyuanSobaIngredientsFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientsFactoryCreateFlavorReturn() throws Exception{
        assertTrue(liyuanSobaIngredientsFactory.createFlavor() instanceof Sweet);
    }

    @Test
    public void testLiyuanSobaIngredientsFactoryCreateMeatReturn() throws Exception{
        assertTrue(liyuanSobaIngredientsFactory.createMeat() instanceof Beef);
    }

    @Test
    public void testLiyuanSobaIngredientsFactoryCreateNoodleReturn() throws Exception{
        assertTrue(liyuanSobaIngredientsFactory.createNoodle() instanceof Soba);
    }

    @Test
    public void testLiyuanSobaIngredientsFactoryCreateToppingReturn() throws Exception{
        assertTrue(liyuanSobaIngredientsFactory.createTopping() instanceof Mushroom);
    }
}