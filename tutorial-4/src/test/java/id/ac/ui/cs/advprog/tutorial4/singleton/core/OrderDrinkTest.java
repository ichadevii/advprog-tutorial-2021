package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class OrderDrinkTest {
    private Class<?> orderDrinkClass;

    @InjectMocks
    private OrderDrink orderDrink;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrinkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink");
        orderDrink = OrderDrink.getInstance();
    }

    @Test
    public void testIsOrderDrinkConcreteClass() {
        assertFalse(Modifier.
                isAbstract(orderDrinkClass.getModifiers()));
    }

    @Test
    public void testOrderDrinkHasGetDrinkMethod() throws Exception {
        Method getDrink = orderDrinkClass.getDeclaredMethod("getDrink");

        assertEquals("java.lang.String",
                getDrink.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDrink.getParameterCount());
        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
    }

    @Test
    public void testOrderDrinkHasSetDrinkMethod() throws Exception {
        Class<?>[] setDrinkArgs = new Class[1];
        setDrinkArgs[0] = String.class;
        Method setDrink = orderDrinkClass.getDeclaredMethod("setDrink", setDrinkArgs);

        assertEquals(1,
                setDrink.getParameterCount());
        assertTrue(Modifier.isPublic(setDrink.getModifiers()));
    }

    @Test
    public void testOrderDrinkHasToStringMethod() throws Exception {
        Method toString = orderDrinkClass.getDeclaredMethod("toString");

        assertEquals("java.lang.String",
                toString.getGenericReturnType().getTypeName());
        assertEquals(0,
                toString.getParameterCount());
        assertTrue(Modifier.isPublic(toString.getModifiers()));
    }

    @Test
    public void testOrderDrinkHasGetInstanceMethod() throws Exception {
        Method getInstance = orderDrinkClass.getDeclaredMethod("getInstance");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink",
                getInstance.getGenericReturnType().getTypeName());
        assertEquals(0,
                getInstance.getParameterCount());
        assertTrue(Modifier.isPublic(getInstance.getModifiers()));
    }

    @Test
    public void testOrderDrinkGetDrinkMethod() {
        orderDrink.setDrink("air putih");
        String result = orderDrink.getDrink() ;
        assertEquals("air putih", result);
    }

    @Test
    public void testOrderDrinkToStringMethod() {
        orderDrink.setDrink("air putih");
        String result = orderDrink.toString() ;
        assertEquals("air putih", result);
    }

    @Test
    public void testOrderDrinkGetInstanceMethod() {
        OrderDrink orderDrink2 = OrderDrink.getInstance();
        assertEquals(orderDrink, orderDrink2);
    }
}
