package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class UmamiTest {
    private Class<?> umamiClass;

    @InjectMocks
    private Umami umami;

    @BeforeEach
    public void setUp() throws Exception {
        umamiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
        umami = new Umami();
    }

    @Test
    public void testIsUmamiConcreteClass() {
        assertFalse(Modifier.
                isAbstract(umamiClass.getModifiers()));
    }

    @Test
    public void testUmamiIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(umamiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

//    public Flavor createFlavor();
//    public Meat createMeat();
//    public Noodle createNoodle();
//    public Topping createTopping();

    @Test
    public void testUmamiOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = umamiClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testUmamiReturnGetDescriptionMethod() {
        String result = umami.getDescription();
        assertEquals("Adding WanPlus Specialty MSG flavoring...", result);
    }

}