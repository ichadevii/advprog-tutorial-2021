package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;

    @InjectMocks
    private InuzumaRamen inuzumaRamen;

//    createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
//    createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
//    createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
//    createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        inuzumaRamen = new InuzumaRamen("WanPlus Beef Mushroom Soba");
    }

    @Test
    public void testIsInuzumaRamenConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenCreateFlavorReturn() throws Exception{
        assertTrue(inuzumaRamen.getFlavor() instanceof Spicy);
    }

    @Test
    public void testInuzumaRamenCreateMeatReturn() throws Exception{
        assertTrue(inuzumaRamen.getMeat() instanceof Pork);
    }

    @Test
    public void testInuzumaRamenCreateNoodleReturn() throws Exception{
        assertTrue(inuzumaRamen.getNoodle() instanceof Ramen);
    }

    @Test
    public void testInuzumaRamenIngredientsFactoryCreateToppingReturn() throws Exception{
        assertTrue(inuzumaRamen.getTopping() instanceof BoiledEgg);
    }
}
