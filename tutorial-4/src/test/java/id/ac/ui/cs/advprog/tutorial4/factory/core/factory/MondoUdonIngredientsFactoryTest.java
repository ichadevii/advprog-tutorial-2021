package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class MondoUdonIngredientsFactoryTest {
    private Class<?> mondoUdonIngredientsFactoryClass;

    @InjectMocks
    private MondoUdonIngredientsFactory mondoUdonIngredientsFactory;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonIngredientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonIngredientsFactory");
        mondoUdonIngredientsFactory = new MondoUdonIngredientsFactory();
    }

    @Test
    public void testIsMondoUdonIngredientsFactoryConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonIngredientsFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientsFactoryIsARamenIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonIngredientsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenIngredientsFactory")));
    }

//    public Flavor createFlavor();
//    public Meat createMeat();
//    public Noodle createNoodle();
//    public Topping createTopping();

    @Test
    public void testMondoUdonIngredientsFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = mondoUdonIngredientsFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientsFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = mondoUdonIngredientsFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientsFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = mondoUdonIngredientsFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientsFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = mondoUdonIngredientsFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientsFactoryCreateFlavorReturn() throws Exception{
        assertTrue(mondoUdonIngredientsFactory.createFlavor() instanceof Salty);
    }

    @Test
    public void testMondoUdonIngredientsFactoryCreateMeatReturn() throws Exception{
        assertTrue(mondoUdonIngredientsFactory.createMeat() instanceof Chicken);
    }

    @Test
    public void testMondoUdonIngredientsFactoryCreateNoodleReturn() throws Exception{
        assertTrue(mondoUdonIngredientsFactory.createNoodle() instanceof Udon);
    }

    @Test
    public void testMondoUdonIngredientsFactoryCreateToppingReturn() throws Exception{
        assertTrue(mondoUdonIngredientsFactory.createTopping() instanceof Cheese);
    }
}