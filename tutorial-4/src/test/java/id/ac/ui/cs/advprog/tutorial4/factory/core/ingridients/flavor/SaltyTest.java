package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class SaltyTest {
    private Class<?> saltyClass;

    @InjectMocks
    private Salty salty;

    @BeforeEach
    public void setUp() throws Exception {
        saltyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty");
        salty = new Salty();
    }

    @Test
    public void testIsSaltyConcreteClass() {
        assertFalse(Modifier.
                isAbstract(saltyClass.getModifiers()));
    }

    @Test
    public void testSaltyIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(saltyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

//    public Flavor createFlavor();
//    public Meat createMeat();
//    public Noodle createNoodle();
//    public Topping createTopping();

    @Test
    public void testSaltyOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = saltyClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testSaltyReturnGetDescriptionMethod() {
        String result = salty.getDescription();
        assertEquals("Adding a pinch of salt...", result);
    }

}