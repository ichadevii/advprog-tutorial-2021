package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: add tests
public class MenuTest {
    private Class<?> menuClass;

    @InjectMocks
    private Menu menu;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
        menu = new MondoUdon("Udon dengan satay 8 tusuk");
    }

    @Test
    public void testIsMenuConcreteClass() {
        assertFalse(Modifier.
                isAbstract(menuClass.getModifiers()));
    }

    @Test
    public void testMenuHasPrepareMethod() throws Exception {
        Method prepare = menuClass.getDeclaredMethod("prepare");

        assertEquals(0,
                prepare.getParameterCount());
        assertTrue(Modifier.isPublic(prepare.getModifiers()));
    }

    @Test
    public void testMenuHasGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testMenuHasGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
    }

    @Test
    public void testMenuHasGetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }

    @Test
    public void testMenuHasGetFlavorMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
    }

    @Test
    public void testMenuHasGetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testMenuGetNameReturn() throws Exception{
        String result = menu.getName();
        assertEquals("Udon dengan satay 8 tusuk", result);
    }

    @Test
    public void testMenuGetNoodleReturn() throws Exception{
        assertTrue(menu.getNoodle() instanceof Udon);
    }

    @Test
    public void testMenuGetMeatReturn() throws Exception{
        assertTrue(menu.getMeat() instanceof Chicken);
    }

    @Test
    public void testMenuGetFlavorReturn() throws Exception{
        assertTrue(menu.getFlavor() instanceof Salty);
    }

    @Test
    public void testMenuGetToppingReturn() throws Exception{
        assertTrue(menu.getTopping() instanceof Cheese);
    }


}
