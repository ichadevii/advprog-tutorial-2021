package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceImplTest {

    @Mock
    private OrderDrink orderDrink = OrderDrink.getInstance();

    @Mock
    private OrderFood orderFood = OrderFood.getInstance();

    @InjectMocks
    private OrderServiceImpl orderService;

    private Class<?> orderServiceClass;

    @BeforeEach
    public void setUp() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderADrinkIsCalledItShouldCallOrderDrinkSetDrinkMethod() {
        orderService.orderADrink("air putih");

        verify(orderDrink, times(1)).setDrink("air putih");
    }

    @Test
    public void testOrderServiceHasGetDrinkMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink",
                getDrink.getGenericReturnType().getTypeName());
        assertEquals(0,
                getDrink.getParameterCount());
        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
    }

    @Test
    public void testOrderAFoodIsCalledItShouldCallOrderFoodSetFoodMethod() {
        orderService.orderAFood("sate");

        verify(orderFood, times(1)).setFood("sate");
    }

    @Test
    public void testOrderServiceHasGetFoodMethod() throws Exception {
        Method getFood = orderServiceClass.getDeclaredMethod("getFood");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood",
                getFood.getGenericReturnType().getTypeName());
        assertEquals(0,
                getFood.getParameterCount());
        assertTrue(Modifier.isPublic(getFood.getModifiers()));
    }

    @Test
    public void testGetFood() {
        OrderFood orderFood2 = orderService.getFood();
        assertNotNull(orderFood2);
    }

    @Test
    public void testGetDrink() {
        OrderDrink orderDrink2 = orderService.getDrink();
        assertNotNull(orderDrink2);
    }

    @Test
    public void testGetDrinkShouldReturnSameInstanceMethod() {
        OrderDrink orderDrink2 = orderService.getDrink() ;
        assertEquals(orderDrink, orderDrink2);
    }

    @Test
    public void testGetFoodShouldReturnSameInstanceMethod() {
        OrderFood orderFood2 = orderService.getFood() ;
        assertEquals(orderFood, orderFood2);
    }

}
