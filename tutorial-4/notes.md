## Singleton Pattern
Menjamin suatu class hanya memiliki satu instance tetapi menyediakan global point access terhadap instance tersebut.

### A. Perbedaan Approach Lazy dan Eager Instatiation
#### Lazy Instantiation
* Instansiasi tidak langsung dilakukan / instance tidak langsung dibuat ketika class diload
* Instansiasi dilakukan didalam method getInstance()
* Instansiasi dilakukan ketika instance dibutuhkan, yaitu saat pemanggilan method getInstance()
* Dengan kata lain, lazy instantiation menunda instansiasi class hingga dibutuhkan

#### Eager Instantiation
* Instansiasi langsung dilakukan / instance langsung dibuat ketika class diload, karena
* Instansiasi dilakukan di instance variable / data field pada class tersebut, di mana instance variable ini akan langsung dibuat / terdefinisi saat class di load
* Sehingga terdapat instance siap pakai tanpa mengetahui instance tersebut akan terpakai atau tidak

### B. Kelebihan dan Kekurangan Approach Lazy dan Eager Instatiation
#### Lazy Instantiation
* Kelebihan
1. Dengan lazy instantiation, jika kita tidak membutuhkan instance, kita melewati object creation yang tidak perlu
2. Digunakan ketika cost dari object creation tinggi sementara penggunaan instance jarang
3. Load time akan jauh lebih kecil dibanding approach yang lain
4. Penggunaan memory yang lebih sedikit
5. Sehingga lazy instantiation (bisa) meningkatkan performance jika digunakan secara proper

* Kekurangan
1. Ketika multi-threading (terjadi ketika banyak yang sedang menggunakan Singleton), maka pada kedua pemanggilan Singleton yang secara bersamaan ini bisa menimbulkan terbuatnya dua instance karena kondisi awal yang sama-sama null dan diinstansiasi bersamaan sehingga perlu digunakan "synchronized"
2. Penggunaan synchronized ketika multi-threading menggunakan banyak resource
3. Untuk mengurangi penggunaan banyak resource, perlu digunakan Double Checked Locking agar penggunaan synchronized bisa dikurangi.
4. Instansiasi yang tertunda bisa menurunkan performance di waktu yang tidak diinginkan

#### Eager Instantiation
* Kelebihan
1. Thread-safety / Eager Instantiation lebih aman karena tidak peduli seberapa banyak thread yang mencoba untuk memanggil getInstance(), akan hanya ada 1 instance yang terbuat sehingga tidak akan melanggar prinsip dari Singleton itu sendiri
2. Kita tidak perlu menulis code secara eksplisit untuk mengatasi multi-threading
3. Tidak ada penundaan instansiasi sehingga tidak ada impact performance akan hal tersebut

* Kekurangan 
1. Menghabiskan memory dan processor time yang bisa menurunkan performance

