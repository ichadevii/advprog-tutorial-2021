package id.ac.ui.cs.advprog.tutorial3.facade.core.enkripsi;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import org.springframework.stereotype.Service;

/*
 * Asumsikan kelas ini sebagai kelas Client
 */
@Service
public class Enkripsi {

    private AbyssalTransformation abyssalTransformation;
    private CelestialTransformation celestialTransformation;
    private CaesarTransformation caesarTransformation;

    public Enkripsi() {
        this.celestialTransformation = new CelestialTransformation();
        this.abyssalTransformation = new AbyssalTransformation();
        this.caesarTransformation = new CaesarTransformation();
    }

    public String encode(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        Spell newSpell;

        Spell hasilCelestical = this.celestialTransformation.encode(spell);
        Spell hasilAbyssal = this.abyssalTransformation.encode(hasilCelestical);
        Spell hasilCaesar = this.caesarTransformation.encode(hasilAbyssal);
        Spell hasilTranslate = CodexTranslator.translate(hasilCaesar, RunicCodex.getInstance());

        newSpell = hasilTranslate;

        return newSpell.getText();
    }

    public String decode(String code) {
        Spell spell = new Spell(code, RunicCodex.getInstance());
        Spell newSpell;

        Spell hasilTranslate = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        Spell hasilCaesar = this.caesarTransformation.decode(hasilTranslate);
        Spell hasilAbyssal = this.abyssalTransformation.decode(hasilCaesar);
        Spell hasilCelestical = this.celestialTransformation.decode(hasilAbyssal);

        newSpell = hasilCelestical;

        return newSpell.getText();
    }
}