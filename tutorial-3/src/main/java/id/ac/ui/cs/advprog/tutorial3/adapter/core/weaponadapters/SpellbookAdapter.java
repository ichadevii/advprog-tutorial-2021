package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean statusChargeAttack;

    public SpellbookAdapter(Spellbook spellbook) {
        super();
        this.spellbook = spellbook;
        this.statusChargeAttack = false;
    }

    @Override
    public String normalAttack() {
        this.statusChargeAttack = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        String spell;
        if (this.statusChargeAttack==false){
            spell = spellbook.largeSpell();
        }
        else{
            spell = "Magic Power not Enough for Large Spell";
        }
        this.statusChargeAttack = true;
        return spell;
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
