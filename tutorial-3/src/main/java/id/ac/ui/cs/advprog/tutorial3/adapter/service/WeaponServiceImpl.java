package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;

    // TODO: implement me

    @Override
    public List<Weapon> findAll() {
//      Add bows to repo
        for (Bow b : bowRepository.findAll()){
            BowAdapter bowAdapter;
            if (weaponRepository.findByAlias(b.getName()) == null) {
                bowAdapter = new BowAdapter(b);
                weaponRepository.save(bowAdapter);
            }
        }

//      Add spellbook to map
        for (Spellbook s : spellbookRepository.findAll()){
            Weapon spellbookAdapter;
            if (weaponRepository.findByAlias(s.getName()) == null) {
                spellbookAdapter = new SpellbookAdapter(s);
                weaponRepository.save(spellbookAdapter);
            }
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        weaponRepository.save(weapon);
        String template = weapon.getHolderName() + " attacked with " + weapon.getName();
        String log;
        String attack;
        if (attackType==0){
            attack = weapon.normalAttack();
            log = template + " (normal attack) " + attack;
        }
        else{
            attack = weapon.chargedAttack();
            log = template + " (charged attack) " + attack;
        }
        this.logRepository.addLog(log);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return this.logRepository.findAll();
    }
}
