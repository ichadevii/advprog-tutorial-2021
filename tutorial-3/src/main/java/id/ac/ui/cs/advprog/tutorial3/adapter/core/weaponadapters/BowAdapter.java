package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

// TODO: complete me :)
public class BowAdapter implements Weapon {
    private Bow bow;
    private boolean statusAimShot;

    public BowAdapter(Bow bow){
        super();
        this.bow = bow;
        this.statusAimShot = false;
    }

    @Override
    public String normalAttack() {
        if (statusAimShot==false){
            statusAimShot = false;
            return bow.shootArrow(false);
        }
        else{
            statusAimShot = true;
            return bow.shootArrow(true);
        }
    }

    @Override
    public String chargedAttack() {
        if (statusAimShot==false){
            statusAimShot = true;
            return bow.shootArrow(true);
        }
        else {
            statusAimShot = false;
            return bow.shootArrow(false);
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
