package id.ac.ui.cs.advprog.tutorial3.facade.core.enkripsi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class EnkripsiTest {
    private Class<?> enkripsiClass;

    @InjectMocks
    private Enkripsi enkripsi;

    @BeforeEach
    public void setup() throws Exception {
        enkripsiClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.enkripsi.Enkripsi");
    }

    @Test
    public void testEnkripsiHasEncodeMethod() throws Exception {
        Method encode = enkripsiClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testEnkripsiEncodeCorrectlyImplemented() {
        String result = enkripsi.encode("Safira and I went to a blacksmith to forge our sword");
        assertEquals("(/^:]ZZ$]{B=![AJJou(?<z((/HcS@X#_([),,xw|zS($%_r%*,p", result);
    }

    @Test
    public void testEnkripsiHasDecodeMethod() throws Exception {
        Method decode = enkripsiClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testEnkripsiDecodeCorrectlyImplemented() {
        String result = enkripsi.decode("(/^:]ZZ$]{B=![AJJou(?<z((/HcS@X#_([),,xw|zS($%_r%*,p");
        assertEquals("Safira and I went to a blacksmith to forge our sword", result);
    }
}